---
layout: handbook-page-toc
title: "Planning throughout the Customer Lifecycle "
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Three Types of Planning

The Field Team creates plans throughout the Customer's lifecycle. First, planning begains at the  Account Plan level, gets more detailed for each opportunity with a Command Plan, then once the deal is signed we collaborate with the customer. 

## Account Planning

**DRI: Strategic Account Leader/Account Executive**

Account Plans are executable and structured plans to help understand the customer's business and  creates a roadmap for a long-term relationship that benefits GitLab and our customers.

To learn more, check out the [Account Plan Page](/handbook/sales/account-planning/).

## Opportunity Planning

**DRI: Strategic Account Leader/Account Executive**

Good Account Plans, simplify the Opportunity Planning since they identify key information and leads to more productive [Opportunity Consults](/handbook/sales/command-of-the-message/opportunity-consults/). 
To learn more check out the [Command Plan page](/handbook/sales/command-of-the-message/command-plan/).

## Success Planning

**DRI: Technical Account Manager**

Success Plans happen after the deal is signed and the Technical Account Manager partners with the Strategic Account Leader/ Account Executive and the Customer to identify their positive business outcomes and milestones to achieve those goals. 
To learn more, check out the [Success Plan Page](/handbook/customer-success/tam/success-plans/).

---
layout: handbook-page-toc
title: "Total Addressable Market"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

Total Addressable Market (TAMkt) is derived by multiplying the estimated number of seats licenses (Users) available by GitLab's potential annual average revenue per user (ARPU).  

| Year | Audience | Users | ARPU | TAMkt |
| ---- | -------- | ----- | ---- | --- |
| 2013 | Enthusiasts | 1m | $20 | 20m |
| 2015 | Developers | 20m | $100 | 2b |
| 2019 | Enterprise | 41m<sup>1</sup> | $350 | 14b |

<sup>1</sup>GitLab projection for users within organizations of more than 2,000 employees

<sup>2</sup>GitLab projection for users accountable for software development, security, and operations

<sup>3</sup>Additional potential users:
 1. Product managers
 1. Management
 1. Compliance
 1. Network engineers
 1. Designers 






---
layout: handbook-page-toc
title: Financial Wrap-up
category: Internal
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Financial Wrap-up
Once SOW items have been delivered and ready for billing and/or revenue recognition, the following should be completed:

## Project Expenses
Before making a purchase of any type or booking travel for a customer project, be sure to obtain approval from your Project Manager or Project Coordinator. The Project Manager or Project Coordinator would need to review if an expense requirement was included in the project SOW.  Once the purchase has incurred or travel expenses booked, please, be sure to follow the process outlined to be sure that expenses are accounted for in the month which incurred. 
1. Purchase incurred or travel booked
1. PSE/PM submit expense report through Expensify with PSE Project Tag and COGS as the category
1. PS Manager approves expense report in Expensify
1. PS Manager selects PS Operations as the next approver for the customer expense report
1. PS Operations then reviews the expense report and sends to Finance approval and processing
1. PS Operations then reviews the customer expense report with the assigned Project Manager
1. PS Operations then adds the expense report to the customer project and submits the billing over to Finance if the expense is billable 

The GitLab Billing Manager will pull an expense report after each month end to be sure no expenses were missed during the submission and approval process.

## Training completion or entire project is complete:
1. Attach the signed Project sign off document or attach the email where the Project sign off document was sent to the customer via email in the PSE Object
1. Update the PSE Object dates and update the status to `Closure`
1. The Project Coordinator will update the PSE Object status to `Complete` once month end is completed and closed with the Finance team

## Milestone completion 
Milestone completion ready for billing and/or revenue recognition, the following should be completed:
1. Attach the signed Project sign off document or attach the email where the Project sign off document was sent to the customer via email in the PSE Object
1. Attach the Trainig Roster to the PSE Object when the training is complete
1. The Project Coordinator will then tag the billing and revenue team in the PSE Object to make Finance aware of completion

## Monthly T&M Hours 
Monthly T&M Hours ready for billing and or revenue recognition, the following should be completed:
1. Project Coordinator pulls the hourly tracking report and add the report to the PSE Object
1. Project Coordinator will then tag the billing and revenue team in the PSE Object to make Finance aware of completion

Projects that contain Milestone or T&M Hourly billing are completed in full the project status should be updated to `Closure` status and project dates updated.
The Project Coordinator will update the PSE Object status to `Complete` once month end is completed and closed with the Finance team.
